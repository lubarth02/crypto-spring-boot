package at.itkolleg.cryptospringboot;

import at.itkolleg.cryptospringboot.api.CryptoApi;
import at.itkolleg.cryptospringboot.domain.Benutzer;
import at.itkolleg.cryptospringboot.domain.Crypto;
import at.itkolleg.cryptospringboot.domain.Wallet;
import at.itkolleg.cryptospringboot.repositories.DbZugriffBenutzer;
import at.itkolleg.cryptospringboot.repositories.DbZugriffCrypto;
import at.itkolleg.cryptospringboot.repositories.DbZugriffWallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class CryptoSpringBootApplication implements ApplicationRunner {
    @Autowired
    DbZugriffCrypto dbZugriffCrypto;

    @Autowired
    DbZugriffBenutzer dbZugriffBenutzer;

    @Autowired
    DbZugriffWallet dbZugriffWallet;

    public static void main(String[] args) {
        SpringApplication.run(CryptoSpringBootApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        List<Crypto> cryptos = CryptoApi.getCryptoFromApi();
        for (Crypto c : cryptos) {
            dbZugriffCrypto.cryptoSpeichern(c);
        }

        Benutzer b1 = new Benutzer("Lukas Barth", "lubarth@tsn.at",20000.0);
        Benutzer b2 = new Benutzer("Max Muster", "max.muster@mail.at",20000.0);
        Benutzer b3 = new Benutzer("Maria Muster", "maria.muster@mail.at", 20000.0);
        dbZugriffBenutzer.benutzerSpeichern(b1);
        dbZugriffBenutzer.benutzerSpeichern(b2);
        dbZugriffBenutzer.benutzerSpeichern(b3);

        Wallet w1 = new Wallet(cryptos.get(0), b1);
        Wallet w2 = new Wallet(cryptos.get(1), b1);
        dbZugriffWallet.walletSpeichern(w1);
        dbZugriffWallet.walletSpeichern(w2);
    }

}

