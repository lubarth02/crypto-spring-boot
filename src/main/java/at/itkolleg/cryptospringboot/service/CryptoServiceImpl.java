package at.itkolleg.cryptospringboot.service;

import at.itkolleg.cryptospringboot.domain.Crypto;
import at.itkolleg.cryptospringboot.exceptions.CryptoDeletionNotPossibleException;
import at.itkolleg.cryptospringboot.exceptions.CryptoNichtGefunden;
import at.itkolleg.cryptospringboot.repositories.DbZugriffCrypto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CryptoServiceImpl implements CryptoService {

    private DbZugriffCrypto dbZugriffCrypto;

    public CryptoServiceImpl(DbZugriffCrypto dbZugriffCrypto) {
        this.dbZugriffCrypto = dbZugriffCrypto;
    }

    @Override
    public Crypto cryptoEinfuegen(Crypto crypto) {
        return this.dbZugriffCrypto.cryptoSpeichern(crypto);
    }

    @Override
    public Crypto cryptoUpdaten(Crypto crypto) throws CryptoNichtGefunden {
        Crypto cryptoAusDb = this.dbZugriffCrypto.cryptoMitId(crypto.getId());
        cryptoAusDb.setName(crypto.getName());
        cryptoAusDb.setPreis(crypto.getPreis());
        return this.dbZugriffCrypto.cryptoSpeichern(crypto);
    }

    @Override
    public List<Crypto> alleCryptos() {
        return this.dbZugriffCrypto.alleCryptos();
    }

    @Override
    public Page<Crypto> alleCryptosPerPage(Pageable pageable) {
        return dbZugriffCrypto.alleCryptosPerPage(pageable);
    }

    @Override
    public List<Crypto> alleCryptosMitPreis(double preis) {
        return this.dbZugriffCrypto.alleCryptosMitPreis(preis);
    }



    @Override
    public List<Crypto> alleCryptosMitName(String name) {
        return this.dbZugriffCrypto.alleCryptosMitName(name);
    }

    @Override
    public Crypto cryptoMitId(Long id) throws CryptoNichtGefunden {
        return this.dbZugriffCrypto.cryptoMitId(id);
    }

    @Override
    public Crypto cryptoLoeschenMitId(Long id) throws CryptoDeletionNotPossibleException {
        try {
            return this.dbZugriffCrypto.cryptoLoeschenMitId(id);
        } catch (Exception e) {
            throw new CryptoDeletionNotPossibleException("Das Löschen ist nicht möglich. Das Crypto könnte noch in einer Wallet verwendet werden");
        }
    }
}

