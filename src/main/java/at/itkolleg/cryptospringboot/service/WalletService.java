package at.itkolleg.cryptospringboot.service;

import at.itkolleg.cryptospringboot.domain.Benutzer;
import at.itkolleg.cryptospringboot.domain.Wallet;
import at.itkolleg.cryptospringboot.exceptions.*;

import java.util.List;

public interface WalletService {

    Wallet cryptoGuthabenHinzufuegen(Wallet wallet) throws InvalidCryptoGuthabenException, WalletNichtGefunden,
            BenutzerNichtGefunden, CryptoNichtGefunden, InvalidBenutzerGuthaben;
    Wallet cryptoGuthabenEntfernen(Wallet wallet) throws WalletNichtGefunden, InvalidCryptoGuthabenException,
            BenutzerNichtGefunden, CryptoNichtGefunden;
    Double walletEuroGuthabenAnzeigen(Long id) throws WalletNichtGefunden, CryptoNichtGefunden;
    List<Wallet> alleWallets();
    Wallet walletMitId(Long id) throws WalletNichtGefunden;
}
