package at.itkolleg.cryptospringboot.service;

import at.itkolleg.cryptospringboot.domain.*;
import at.itkolleg.cryptospringboot.exceptions.*;
import at.itkolleg.cryptospringboot.repositories.DbZugriffCrypto;
import at.itkolleg.cryptospringboot.repositories.DbZugriffBenutzer;
import at.itkolleg.cryptospringboot.repositories.DbZugriffWallet;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WalletServiceImpl implements WalletService{

    DbZugriffWallet dbZugriffWallet;
    DbZugriffCrypto dbZugriffCrypto;
    DbZugriffBenutzer dbZugriffBenutzer;

    public WalletServiceImpl(DbZugriffWallet dbZugriffWallet, DbZugriffCrypto dbZugriffCrypto, DbZugriffBenutzer dbZugriffBenutzer){
        this.dbZugriffWallet = dbZugriffWallet;
        this.dbZugriffCrypto = dbZugriffCrypto;
        this.dbZugriffBenutzer = dbZugriffBenutzer;
    }

    //ich brauche Crypto und Benutzer und im thymleaf auch, in allWallets und in cryptoKaufen
    @Override
    public Wallet cryptoGuthabenHinzufuegen(Wallet wallet) throws InvalidCryptoGuthabenException,
            WalletNichtGefunden, BenutzerNichtGefunden, CryptoNichtGefunden, InvalidBenutzerGuthaben {

        Wallet walletAusDb = this.dbZugriffWallet.walletMitId(wallet.getId());
        Benutzer benutzer = this.dbZugriffBenutzer.benutzerMitId(wallet.getBenutzer().getId());
        Crypto crypto = this.dbZugriffCrypto.cryptoMitId(wallet.getCrypto().getId());
        //walletAusDb.setCryptoGuthaben(wallet.getCryptoGuthaben());
        //walletAusDb.setCrypto(wallet.getCrypto());
        //walletAusDb.setBenutzer(wallet.getBenutzer());
        //walletAusDb.setName(wallet.getName());
        if(benutzer.getGuthaben() > wallet.getCryptoGuthaben() * crypto.getPreis()){
            Double derzeitigesGuthaben = benutzer.getGuthaben();
            benutzer.setGuthaben(derzeitigesGuthaben - (wallet.getCryptoGuthaben() * crypto.getPreis()));
            wallet.addCryptoGuthaben(walletAusDb.getCryptoGuthaben());
            wallet.setBenutzer(benutzer);
            //dbZugriffBenutzer.benutzerSpeichern(benutzer);
            return dbZugriffWallet.walletSpeichern(wallet);
        }else{
            throw new InvalidBenutzerGuthaben("Nicht genug Guthaben zur Verfügung!");
        }





        /*Double euroGuthaben = benutzer.getGuthaben(); //Versuch benutzer und crypto sind null
            if(euroGuthaben >= (crypto.getPreis())) {
                Double derzeitigesCryptoGuthaben = walletAusDb.getCryptoGuthaben();
                walletAusDb.addCryptoGuthaben(wallet.getCryptoGuthaben());

                //benutzer.setGuthaben(euroGuthaben - (cryptoGuthaben * crypto.getPreis()));
                //dbZugriffBenutzer.benutzerSpeichern(benutzer);
                //wallet.setBenutzer(benutzer);
                return dbZugriffWallet.walletSpeichern(wallet);
            }else{
                wallet.addCryptoGuthaben(wallet.getCryptoGuthaben());
                return dbZugriffWallet.walletSpeichern(wallet);
            } */
    }

    @Override
    public Wallet cryptoGuthabenEntfernen(Wallet wallet) throws WalletNichtGefunden, BenutzerNichtGefunden, CryptoNichtGefunden, InvalidCryptoGuthabenException {
        Wallet walletAusDb = this.dbZugriffWallet.walletMitId(wallet.getId());
        Benutzer benutzer = this.dbZugriffBenutzer.benutzerMitId(wallet.getBenutzer().getId());
        Crypto crypto = this.dbZugriffCrypto.cryptoMitId(wallet.getCrypto().getId());

        if(walletAusDb.getCryptoGuthaben() >= wallet.getCryptoGuthaben()) {
            Double derzeitigesGuthaben = benutzer.getGuthaben();
            benutzer.setGuthaben(derzeitigesGuthaben + (wallet.getCryptoGuthaben() * crypto.getPreis()));
            wallet.setCryptoGuthaben(walletAusDb.getCryptoGuthaben() - wallet.getCryptoGuthaben());
            wallet.setBenutzer(benutzer);
            return dbZugriffWallet.walletSpeichern(wallet);
        }else{
            throw new  InvalidCryptoGuthabenException("Du besitzt nicht so viele Cryptos!");
        }
        /*Wallet wallet = dbZugriffWallet.walletMitId(id);
        Benutzer benutzer = wallet.getBenutzer();
        Crypto crypto = wallet.getCrypto();
        if(wallet.getCryptoGuthaben() >= cryptoGuthaben){
            wallet.setCryptoGuthaben(wallet.getCryptoGuthaben() - cryptoGuthaben);
            benutzer.setGuthaben(benutzer.getGuthaben() + (crypto.getPreis() * cryptoGuthaben));
            dbZugriffBenutzer.benutzerSpeichern(benutzer);
            dbZugriffWallet.walletSpeichern(wallet); */

        }


    @Override
    public Double walletEuroGuthabenAnzeigen(Long id) throws WalletNichtGefunden, CryptoNichtGefunden {
        Wallet wallet = dbZugriffWallet.walletMitId(id);
        Crypto crypto = dbZugriffCrypto.cryptoMitId(wallet.getCrypto().getId());
        Double preis = crypto.getPreis();
        Double cryptoGuthaben = wallet.getCryptoGuthaben();
        Double walletWertInEuro = preis * cryptoGuthaben;
        return Math.round(walletWertInEuro * 100.0) /100.0;
    }

    public List<Wallet> alleWallets(){
        return dbZugriffWallet.alleWallets();
    }

    public Wallet walletMitId(Long id) throws WalletNichtGefunden{
        return dbZugriffWallet.walletMitId(id);
    }
}
