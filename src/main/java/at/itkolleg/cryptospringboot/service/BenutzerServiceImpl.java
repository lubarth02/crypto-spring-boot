package at.itkolleg.cryptospringboot.service;

import at.itkolleg.cryptospringboot.domain.Benutzer;
import at.itkolleg.cryptospringboot.exceptions.BenutzerNichtGefunden;
import at.itkolleg.cryptospringboot.repositories.DbZugriffBenutzer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BenutzerServiceImpl implements BenutzerService {

    private DbZugriffBenutzer dbZugriffBenutzer;

    public BenutzerServiceImpl(DbZugriffBenutzer dbZugriffBenutzer) {
        this.dbZugriffBenutzer = dbZugriffBenutzer;
    }

    @Override
    public Benutzer benutzerEinfuegen(Benutzer benutzer) {
        return this.dbZugriffBenutzer.benutzerSpeichern(benutzer);
    }

    @Override
    public Benutzer benutzerUpdaten(Benutzer benutzer) throws BenutzerNichtGefunden {
        Benutzer benutzerAusDb = this.dbZugriffBenutzer.benutzerMitId(benutzer.getId());
        benutzerAusDb.setName(benutzer.getName());
        benutzerAusDb.setEmail(benutzer.getEmail());
        return this.dbZugriffBenutzer.benutzerSpeichern(benutzer);
    }

    @Override
    public List<Benutzer> alleBenutzer() {
        return this.dbZugriffBenutzer.alleBenutzer();
    }

    @Override
    public List<Benutzer> alleBenutzerMitName(String name) {
        return this.dbZugriffBenutzer.benutzerMitName(name);
    }

    @Override
    public Benutzer benutzerMitId(Long id) throws BenutzerNichtGefunden {
        return this.dbZugriffBenutzer.benutzerMitId(id);
    }

    @Override
    public Benutzer benutzerMitIdLoeschen(Long id) throws BenutzerNichtGefunden {
        return this.dbZugriffBenutzer.benutzerLoeschenMitId(id);
    }
}


