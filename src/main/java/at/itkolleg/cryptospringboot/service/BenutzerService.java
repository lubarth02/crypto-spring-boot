package at.itkolleg.cryptospringboot.service;

import at.itkolleg.cryptospringboot.domain.Benutzer;
import at.itkolleg.cryptospringboot.exceptions.BenutzerNichtGefunden;

import java.util.List;

public interface BenutzerService {

    Benutzer benutzerEinfuegen(Benutzer benutzer);
    Benutzer benutzerUpdaten(Benutzer benutzer) throws BenutzerNichtGefunden;
    List<Benutzer> alleBenutzer();
    List<Benutzer> alleBenutzerMitName(String name);
    Benutzer benutzerMitId(Long id) throws BenutzerNichtGefunden;
    Benutzer benutzerMitIdLoeschen(Long id) throws BenutzerNichtGefunden;
}
