package at.itkolleg.cryptospringboot.api;

import at.itkolleg.cryptospringboot.domain.Crypto;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import at.itkolleg.cryptospringboot.exceptions.InvalidPreisException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


public class CryptoApi {

    public static List<Crypto> getCryptoFromApi() throws IOException, InvalidPreisException {
        List<Crypto> cryptoList = new ArrayList<>();

        String url = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=eur&order=market_cap_desc&per_page=100&page=1&sparkline=false";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        int responseCode = con.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            Scanner in = new Scanner(con.getInputStream());
            StringBuilder response = new StringBuilder();
            while (in.hasNextLine()) {
                response.append(in.nextLine());
            }
            in.close();

            String responseBody = response.toString();

            // zurückgeliefertes JSON wird in Crypto Objekt umgewandelt
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode rootNode = objectMapper.readTree(responseBody);
            for (JsonNode node : rootNode) {
                String name = node.get("name").asText();
                double currentPrice = node.get("current_price").asDouble();
                Crypto crypto = new Crypto(name, currentPrice);
                cryptoList.add(crypto);
            }
            return cryptoList;
        } else {
            System.out.println("Error: " + responseCode);
            return null;
        }
    }
}


