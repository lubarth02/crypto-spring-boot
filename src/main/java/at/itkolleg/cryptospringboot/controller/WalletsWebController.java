package at.itkolleg.cryptospringboot.controller;

import at.itkolleg.cryptospringboot.domain.Benutzer;
import at.itkolleg.cryptospringboot.domain.Crypto;
import at.itkolleg.cryptospringboot.domain.Wallet;
import at.itkolleg.cryptospringboot.exceptions.*;
import at.itkolleg.cryptospringboot.service.BenutzerService;
import at.itkolleg.cryptospringboot.service.CryptoService;
import at.itkolleg.cryptospringboot.service.WalletService;
import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/web/v1/wallets")
public class WalletsWebController {

    private WalletService walletService;
    private BenutzerService benutzerService;
    private CryptoService cryptoService;

    public WalletsWebController(WalletService walletService, BenutzerService benutzerService, CryptoService cryptoService){
        this.walletService = walletService;
        this.benutzerService = benutzerService;
        this.cryptoService = cryptoService;
    }

    @GetMapping
    public String gibAlleWallets(Model model) {
        model.addAttribute("allWallets", this.walletService.alleWallets());
        return "allWallets";
    }

    @GetMapping("/buycrypto/{id}")
    public String cryptoKaufenFormular(@PathVariable Long id, Model model){
        try{
            Wallet wallet = this.walletService.walletMitId(id);
            model.addAttribute("wallet", wallet);
            return "cryptoKaufen";
        } catch (WalletNichtGefunden walletNichtGefunden){
            return "redirect:/web/v1/wallets";
        }
    }

    @PostMapping("/buycrypto")
    public String cryptoKaufen(@Valid Wallet wallet, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            return "cryptoKaufen";
        }else {
            try{
                this.walletService.cryptoGuthabenHinzufuegen(wallet);
                return "redirect:/web/v1/wallets";
            }catch (InvalidCryptoGuthabenException invalidCryptoGuthabenException){
                return "redirect:/web/v1/wallets"; //bessere Lösung
            }catch (WalletNichtGefunden | CryptoNichtGefunden | BenutzerNichtGefunden walletNichtGefunden){
                return "redirect:/web/v1/wallets";
            }catch (InvalidBenutzerGuthaben invalidBenutzerGuthaben){
                return "cryptoKaufen";
            }
        }
    }

    @GetMapping("/sellcrypto/{id}")
    public String cryptoVerkaufenFormular(@PathVariable Long id, Model model){
        try{
            Wallet wallet = this.walletService.walletMitId(id);
            model.addAttribute("wallet", wallet);
            return "cryptoVerkaufen";
        } catch (WalletNichtGefunden walletNichtGefunden){
            return "redirect:/web/v1/wallets";
        }
    }

    @PostMapping("/sellcrypto")
    public String cryptoVerkaufen(@Valid Wallet wallet, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            return "cryptoVerkaufen";
        }else {
            try{
                this.walletService.cryptoGuthabenEntfernen(wallet);
                return "redirect:/web/v1/wallets";
            }catch (InvalidCryptoGuthabenException invalidCryptoGuthabenException){
                return "cryptoVerkaufen"; //bessere Lösung
            }catch (WalletNichtGefunden | CryptoNichtGefunden | BenutzerNichtGefunden walletNichtGefunden){
                return "redirect:/web/v1/wallets";
            }
        }
    }




}
