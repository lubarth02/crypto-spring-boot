package at.itkolleg.cryptospringboot.controller;

import at.itkolleg.cryptospringboot.domain.Benutzer;
import at.itkolleg.cryptospringboot.exceptions.BenutzerNichtGefunden;
import at.itkolleg.cryptospringboot.service.BenutzerService;
import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/web/v1/benutzer")

public class BenutzerWebController {

    private BenutzerService benutzerService;

    public BenutzerWebController(BenutzerService benutzerService) {
        this.benutzerService = benutzerService;
    }

    @GetMapping
    public String gibAlleBenutzer(Model model) {
        //alle Benutzer werden dem Model hinzugefügt
        model.addAttribute("allUsers", this.benutzerService.alleBenutzer());
        return "alleBenutzer";
    }

    @GetMapping("/insert")
    public String benutzerEinfuegenFormular(Model model) {
        Benutzer benutzer = new Benutzer();
        model.addAttribute("benutzer", benutzer);
        return "benutzerEinfuegen";
    }

    @PostMapping("/insert")
    public String benutzerEinfuegen(@Valid Benutzer benutzer, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "benutzerEinfuegen";
        } else {
            this.benutzerService.benutzerEinfuegen(benutzer);
            return "redirect:/web/v1/benutzer"; //redirect auf Startseite bei erfolgreichem einfügen
        }
    }

    @GetMapping("/update/{id}")
    public String benutzerUpdatenFormular(@PathVariable Long id, Model model){
        try {
            Benutzer benutzer = this.benutzerService.benutzerMitId(id);
            model.addAttribute("benutzer", benutzer);
            return "benutzerUpdaten";
        } catch (BenutzerNichtGefunden benutzerNichtGefunden) {
            return "redirect:/web/v1/benutzer";
        }
    }

    @PostMapping("/update")
    public String benutzerUpdaten(@Valid Benutzer benutzer, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "benutzerUpdaten";
        } else {
            try {
                this.benutzerService.benutzerUpdaten(benutzer);
                return "redirect:/web/v1/benutzer";
            } catch (BenutzerNichtGefunden benutzerNichtGefunden) {
                return "redirect:/web/v1/benutzer";
            }
        }
    }

    @GetMapping("/delete/{id}")
    public String benutzerLoeschen(@PathVariable Long id) {
        try {
            this.benutzerService.benutzerMitIdLoeschen(id);
            return "redirect:/web/v1/benutzer";
        } catch (BenutzerNichtGefunden benutzerNichtGefunden) {
            return "redirect:/web/v1/benutzer";
        }

    }
}


