package at.itkolleg.cryptospringboot.controller;

import at.itkolleg.cryptospringboot.domain.Benutzer;
import at.itkolleg.cryptospringboot.domain.Crypto;
import at.itkolleg.cryptospringboot.exceptions.*;
import at.itkolleg.cryptospringboot.service.CryptoService;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/crypto")
@CrossOrigin(origins = "http://127.0.0.1:5500")

public class CryptoRestController {

    private CryptoService cryptoService;

    public CryptoRestController(CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }

    @GetMapping
    public ResponseEntity<List<Crypto>>gibAlleCryptos(){
        return ResponseEntity.ok(this.cryptoService.alleCryptos());
    }

    @GetMapping(value = "/perPage", params = {"page", "size"})
    public ResponseEntity<Page<Crypto>> gibAlleCryptosSeitenweise(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok(this.cryptoService.alleCryptosPerPage(PageRequest.of(page, size)));
    }

    @PostMapping
    public ResponseEntity<Crypto> cryptoEinfuegen(@Valid @RequestBody Crypto crypto, BindingResult bindingResult) throws CryptoValidierungFehlgeschlagen {
        //    String errors = "";
        FormValidierungExceptionDTO formValidationErrors = new FormValidierungExceptionDTO("9000");

        if (bindingResult.hasErrors()) {
            for (ObjectError error : bindingResult.getAllErrors()) {
                formValidationErrors.addFormValidationError(((FieldError) error).getField(), error.getDefaultMessage());
            }
            throw new CryptoValidierungFehlgeschlagen(formValidationErrors);
        } else {
            System.out.println("NAME: " + crypto.getName());
            return ResponseEntity.ok(this.cryptoService.cryptoEinfuegen(crypto));
        }
    }

    @PutMapping
    public ResponseEntity<Crypto> cryptoUpdaten(@Valid @RequestBody Crypto crypto, BindingResult bindingResult) throws CryptoNichtGefunden, CryptoValidierungFehlgeschlagen {
        FormValidierungExceptionDTO formValidationErrors = new FormValidierungExceptionDTO("9000");

        if (crypto.getId() == null)
            throw new CryptoNichtGefunden("Crypto-Update ohne ID nicht möglich!");

        if (bindingResult.hasErrors()) {
            for (ObjectError error : bindingResult.getAllErrors()) {
                formValidationErrors.addFormValidationError(((FieldError) error).getField(), error.getDefaultMessage());
            }
            throw new CryptoValidierungFehlgeschlagen(formValidationErrors);
        } else {
            return ResponseEntity.ok(this.cryptoService.cryptoUpdaten(crypto));
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Crypto> cryptoLoeschen(@PathVariable Long id) throws CryptoDeletionNotPossibleException {
        try {
            return ResponseEntity.ok(this.cryptoService.cryptoLoeschenMitId(id));
        } catch (Exception e) {
            throw new CryptoDeletionNotPossibleException("Das Löschen war nicht möglich. Das Crypto könnte noch in einer Wallet sein");
        }
    }

    @GetMapping("/mitname/{name}")
    public ResponseEntity<List<Crypto>> alleCryptoMitName(@PathVariable String name) {
        return ResponseEntity.ok(this.cryptoService.alleCryptosMitName(name));
    }

    @GetMapping("/mitpreis/{preis}")
    public ResponseEntity<List<Crypto>> alleCryptoMitPreis(@PathVariable double preis) {
        return ResponseEntity.ok(this.cryptoService.alleCryptosMitPreis(preis));
    }


}
