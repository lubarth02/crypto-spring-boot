package at.itkolleg.cryptospringboot.controller;

import at.itkolleg.cryptospringboot.domain.Benutzer;
import at.itkolleg.cryptospringboot.exceptions.BenutzerNichtGefunden;
import at.itkolleg.cryptospringboot.exceptions.BenutzerValidierungFehlgeschlagen;
import at.itkolleg.cryptospringboot.exceptions.FormValidierungExceptionDTO;
import at.itkolleg.cryptospringboot.service.BenutzerService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/v1/benutzer")
public class BenutzerRestController {

    private BenutzerService benutzerService;

    public BenutzerRestController(BenutzerService benutzerService) {
        this.benutzerService = benutzerService;
    }

    @GetMapping
    public ResponseEntity<List<Benutzer>> getAlleBenutzer() {
        return ResponseEntity.ok(this.benutzerService.alleBenutzer());
    }

    @GetMapping("/mitName/{name}")
    public ResponseEntity<List<Benutzer>> getBenutzerMitNamen(String name){
        return ResponseEntity.ok(this.benutzerService.alleBenutzerMitName(name));
    }

    @PostMapping
    public ResponseEntity<Benutzer> studentEinfuegen(@Valid @RequestBody Benutzer benutzer, BindingResult bindingResult) throws BenutzerValidierungFehlgeschlagen {
        //    String errors = "";
        FormValidierungExceptionDTO formValidationErrors = new FormValidierungExceptionDTO("9000");

        if (bindingResult.hasErrors()) {
            for (ObjectError error : bindingResult.getAllErrors()) {
                formValidationErrors.addFormValidationError(((FieldError) error).getField(), error.getDefaultMessage());
            }
            throw new BenutzerValidierungFehlgeschlagen(formValidationErrors);
        } else {
            System.out.println("NAME: " + benutzer.getName());
            return ResponseEntity.ok(this.benutzerService.benutzerEinfuegen(benutzer));
        }
    }

    @PutMapping
    public ResponseEntity<Benutzer> benutzerUpdaten(@Valid @RequestBody Benutzer benutzer, BindingResult bindingResult) throws BenutzerNichtGefunden, BenutzerValidierungFehlgeschlagen {
        FormValidierungExceptionDTO formValidationErrors = new FormValidierungExceptionDTO("9000");

        if (benutzer.getId() == null)
            throw new BenutzerNichtGefunden("Benutzer-Update ohne ID nicht möglich!");

        if (bindingResult.hasErrors()) {
            for (ObjectError error : bindingResult.getAllErrors()) {
                formValidationErrors.addFormValidationError(((FieldError) error).getField(), error.getDefaultMessage());
            }
            throw new BenutzerValidierungFehlgeschlagen(formValidationErrors);
        } else {
            return ResponseEntity.ok(this.benutzerService.benutzerUpdaten(benutzer));
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Benutzer> benutzerLoeschen(@PathVariable Long id) throws BenutzerNichtGefunden {
        return ResponseEntity.ok(this.benutzerService.benutzerMitIdLoeschen(id));
    }




}


