package at.itkolleg.cryptospringboot.controller;

import at.itkolleg.cryptospringboot.domain.Crypto;
import at.itkolleg.cryptospringboot.exceptions.CryptoDeletionNotPossibleException;
import at.itkolleg.cryptospringboot.exceptions.CryptoNichtGefunden;
import at.itkolleg.cryptospringboot.service.CryptoService;
import jakarta.validation.Valid;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/web/v1/crypto")
public class CryptoWebController {

    CryptoService cryptoService;

    public CryptoWebController(CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }

    @GetMapping
    public String gibAlleCryptos(Model model){
        model.addAttribute("alleCryptos", this.cryptoService.alleCryptos());
        return "allcryptos";
    }

    @GetMapping("/perPage")
    public String gibAlleCryptosSeitenweise(Model model, @RequestParam("page") int page, @RequestParam("size") int size) {

        int pageNumber = (page < 0) ? page : 0;
        int sizePerPage = (size < 0) ? size : 5;
        model.addAttribute("alleCryptosSeitenweise", this.cryptoService.alleCryptosPerPage(PageRequest.of(pageNumber, sizePerPage)));
        int totalPages = this.cryptoService.alleCryptosPerPage(PageRequest.of(page, size)).getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "allcryptosPerPage";
    }

    @GetMapping("/insert")
    public String insertcryptoform(Model model) {
        Crypto crypto = new Crypto();
        model.addAttribute("crypto", crypto);
        return "insertcrypto";
    }

    @PostMapping("/insert")
    public String insertCrypto(@Valid Crypto crypto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "insertcrypto";
        } else {
            this.cryptoService.cryptoEinfuegen(crypto);
            return "redirect:/web/v1/crypto";
        }
    }

    @GetMapping("/update/{id}")
    public String cryptoUpdatenFormular(@PathVariable Long id, Model model) {
        try {
            Crypto crypto = this.cryptoService.cryptoMitId(id);
            model.addAttribute("crypto", crypto);
            return "cryptoupdaten";
        } catch (CryptoNichtGefunden cryptoNichtGefunden) {
            return "redirect:/web/v1/crypto";
        }
    }

    @PostMapping("/update")
    public String cryptoUpdaten(Crypto crypto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "cryptoupdaten";
        } else {
            try {
                this.cryptoService.cryptoUpdaten(crypto);
                return "redirect:/web/v1/crypto";
            } catch (CryptoNichtGefunden cryptoNichtGefunden) {
                return "redirect:/web/v1/crypto";
            }
        }
    }

    @GetMapping("/delete/{id}")
    public String cryptoLoeschen(@PathVariable Long id, Model model) throws CryptoNichtGefunden {
        try {
            this.cryptoService.cryptoLoeschenMitId(id);
            return "redirect:/web/v1/crypto";
        }  catch (CryptoDeletionNotPossibleException e)
        {
            model.addAttribute("errortitle", "Crypto-Löschen schlägt fehl!");
            model.addAttribute("errormessage", e.getMessage());
            return "myerrorspage";
        }

    }


}

