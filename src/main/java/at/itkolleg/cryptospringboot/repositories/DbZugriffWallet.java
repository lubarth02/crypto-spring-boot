package at.itkolleg.cryptospringboot.repositories;

import at.itkolleg.cryptospringboot.domain.Crypto;
import at.itkolleg.cryptospringboot.domain.CryptoGuthaben;
import at.itkolleg.cryptospringboot.domain.Wallet;
import at.itkolleg.cryptospringboot.exceptions.InvalidCryptoGuthabenException;
import at.itkolleg.cryptospringboot.exceptions.WalletNichtGefunden;

import java.util.List;

public interface DbZugriffWallet {
    Wallet walletSpeichern(Wallet wallet); //der User sollte nicht in der Lage sein selber wallets zu Speichern
    Wallet walletLoeschenMitId(Long id) throws WalletNichtGefunden;
    List<Wallet> alleWallets();
    Wallet walletMitId(Long id) throws WalletNichtGefunden;

    //Wallet cryptoGuthabenHinzufuegenMitId(Long id, CryptoGuthaben cryptoGuthaben) throws WalletNichtGefunden, InvalidCryptoGuthabenException; //braucht man User dafür?
    //Wallet cryptoGuthabenEntfernenMitId(Long id, CryptoGuthaben cryptoGuthaben); //braucht man User dafür?
}
