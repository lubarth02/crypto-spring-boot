package at.itkolleg.cryptospringboot.repositories;

import at.itkolleg.cryptospringboot.domain.Benutzer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BenutzerJPARepo extends JpaRepository<Benutzer,Long> {
    List<Benutzer> findBenutzerByNameIsLikeIgnoreCase(String name);
}


