package at.itkolleg.cryptospringboot.repositories;

import at.itkolleg.cryptospringboot.domain.Benutzer;
import at.itkolleg.cryptospringboot.exceptions.BenutzerNichtGefunden;

import java.util.List;

public interface DbZugriffBenutzer {
    Benutzer benutzerSpeichern(Benutzer benutzer);
    List<Benutzer> alleBenutzer();
    Benutzer benutzerMitId(Long id) throws BenutzerNichtGefunden;
    Benutzer benutzerLoeschenMitId(Long id) throws BenutzerNichtGefunden;
    List<Benutzer> benutzerMitName(String name);
}




