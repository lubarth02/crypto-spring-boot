package at.itkolleg.cryptospringboot.repositories;

import at.itkolleg.cryptospringboot.domain.Benutzer;
import at.itkolleg.cryptospringboot.exceptions.BenutzerNichtGefunden;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
@Component
public class DbZugriffBenutzerJPA implements DbZugriffBenutzer {

    private BenutzerJPARepo benutzerJPARepo;

    public DbZugriffBenutzerJPA(BenutzerJPARepo benutzerJPARepo) {
        this.benutzerJPARepo = benutzerJPARepo;
    }

    @Override
    public Benutzer benutzerSpeichern(Benutzer benutzer) {
        return benutzerJPARepo.save(benutzer);
    }

    @Override
    public List<Benutzer> alleBenutzer() {
        return benutzerJPARepo.findAll();
    }

    @Override
    public Benutzer benutzerMitId(Long id) throws BenutzerNichtGefunden {
        Optional<Benutzer> optionalUser = this.benutzerJPARepo.findById(id);
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        } else {
            throw new BenutzerNichtGefunden("Der User mit der id" + id + " wurde nicht gefunden");
        }
    }

    @Override
    public Benutzer benutzerLoeschenMitId(Long id) throws BenutzerNichtGefunden {
        Benutzer benutzerAusDb = this.benutzerMitId(id);
        this.benutzerJPARepo.deleteById(benutzerAusDb.getId());
        return benutzerAusDb;
    }

    @Override
    public List<Benutzer> benutzerMitName(String name) {
        return this.benutzerJPARepo.findBenutzerByNameIsLikeIgnoreCase(name);
    }
}

