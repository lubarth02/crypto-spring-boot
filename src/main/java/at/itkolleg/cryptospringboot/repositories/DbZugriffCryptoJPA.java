package at.itkolleg.cryptospringboot.repositories;

import at.itkolleg.cryptospringboot.domain.Crypto;
import at.itkolleg.cryptospringboot.exceptions.CryptoDeletionNotPossibleException;
import at.itkolleg.cryptospringboot.exceptions.CryptoNichtGefunden;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class DbZugriffCryptoJPA implements DbZugriffCrypto {
    private CryptoJPARepo cryptoJPARepo;

    public DbZugriffCryptoJPA(CryptoJPARepo cryptoJPARepo) {
        this.cryptoJPARepo = cryptoJPARepo;
    }

    @Override
    public Crypto cryptoSpeichern(Crypto crypto) {
        return this.cryptoJPARepo.save(crypto);
    }

    @Override
    public List<Crypto> alleCryptos() {
        return cryptoJPARepo.findAll();
    }

    @Override
    public Page<Crypto> alleCryptosPerPage(Pageable pageable) {
        return cryptoJPARepo.findAll(pageable);
    }


    @Override
    public List<Crypto> alleCryptosMitPreis(double preis) {
        return this.cryptoJPARepo.findCryptosByPreis(preis);
    }

    @Override
    public List<Crypto> alleCryptosMitName(String name) {
        return this.cryptoJPARepo.findCryptosByNameLike(name);
    }

    @Override
    public Crypto cryptoMitId(Long id) throws CryptoNichtGefunden {
        Optional<Crypto> optionalCrypto = this.cryptoJPARepo.findById(id);
        if (optionalCrypto.isPresent()) {
            return optionalCrypto.get();
        } else {
            throw new CryptoNichtGefunden("Die Cryptowährung mit der id" + id + " wurde nicth gefunden");
        }
    }

    @Override
    public Crypto cryptoLoeschenMitId(Long id) throws CryptoDeletionNotPossibleException {
        try {
            Crypto cryptotAusDb = this.cryptoMitId(id);
            this.cryptoJPARepo.deleteById(cryptotAusDb.getId());
            return cryptotAusDb;
        } catch (Exception e) {
            System.out.println(e.getCause() + e.getClass().getName());
            throw new CryptoDeletionNotPossibleException("Crypto konnte nicht gelöscht werden! Mögliche Ursache: Das Crypto kann nicht gelöscht werden, da es in einer Wallet ist!");
        }

    }
}

