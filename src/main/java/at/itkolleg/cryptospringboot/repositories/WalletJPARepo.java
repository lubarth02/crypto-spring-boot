package at.itkolleg.cryptospringboot.repositories;

import at.itkolleg.cryptospringboot.domain.Crypto;
import at.itkolleg.cryptospringboot.domain.Benutzer;
import at.itkolleg.cryptospringboot.domain.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WalletJPARepo extends JpaRepository<Wallet,Long> {

    List<Wallet> findWalletByCrypto(Crypto crypto);
    List<Wallet> findWalletsByBenutzer(Benutzer benutzer);


}
