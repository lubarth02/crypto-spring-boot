package at.itkolleg.cryptospringboot.repositories;

import at.itkolleg.cryptospringboot.domain.Crypto;
import at.itkolleg.cryptospringboot.exceptions.CryptoDeletionNotPossibleException;
import at.itkolleg.cryptospringboot.exceptions.CryptoNichtGefunden;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DbZugriffCrypto {
    Crypto cryptoSpeichern(Crypto crypto);
    List<Crypto> alleCryptos();
    Page<Crypto>alleCryptosPerPage(Pageable pageable);
    List<Crypto> alleCryptosMitPreis(double preis);
    List<Crypto> alleCryptosMitName(String name);
    Crypto cryptoMitId(Long id) throws CryptoNichtGefunden;
    Crypto cryptoLoeschenMitId(Long id) throws CryptoNichtGefunden, CryptoDeletionNotPossibleException;
}


