package at.itkolleg.cryptospringboot.repositories;

import at.itkolleg.cryptospringboot.domain.Crypto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CryptoJPARepo extends JpaRepository<Crypto,Long> {
    Page<Crypto> findAll(Pageable pageable);
    List<Crypto> findCryptosByPreis(double preis);
    List<Crypto> findCryptosByNameLike(String name);
}



