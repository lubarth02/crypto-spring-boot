package at.itkolleg.cryptospringboot.repositories;

import at.itkolleg.cryptospringboot.domain.CryptoGuthaben;
import at.itkolleg.cryptospringboot.domain.Wallet;
import at.itkolleg.cryptospringboot.exceptions.InvalidCryptoGuthabenException;
import at.itkolleg.cryptospringboot.exceptions.WalletNichtGefunden;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class DbZugriffWalletJPA implements DbZugriffWallet{

    private WalletJPARepo walletJPARepo;

    public DbZugriffWalletJPA(WalletJPARepo walletJPARepo){
        this.walletJPARepo = walletJPARepo;
    }

    @Override
    public Wallet walletSpeichern(Wallet wallet) {
        return this.walletJPARepo.save(wallet);
    }

    @Override
    public Wallet walletLoeschenMitId(Long id) throws WalletNichtGefunden {
        Optional<Wallet> optionalWallet = this.walletJPARepo.findById(id);
        if(optionalWallet.isPresent()){
            Wallet wallet = walletMitId(id);
            this.walletJPARepo.deleteById(id);
            return wallet;
        }else {
            throw new WalletNichtGefunden("Das Wallet mit der id" + id + " wurde nicth gefunden");
        }
    }

    @Override
    public List<Wallet> alleWallets() {
        return this.walletJPARepo.findAll();
    }

    public Wallet walletMitId(Long id) throws WalletNichtGefunden{
        Optional<Wallet> optionalWallet = this.walletJPARepo.findById(id);
        if(optionalWallet.isPresent()){
            return optionalWallet.get();
        }else {
            throw new WalletNichtGefunden("Das Wallet mit der id" + id + " wurde nicth gefunden");
        }
    }


    /*public Wallet cryptoGuthabenHinzufuegenMitId(Long id, CryptoGuthaben cryptoGuthaben) throws WalletNichtGefunden, InvalidCryptoGuthabenException {
        Optional<Wallet> optionalWallet = this.walletJPARepo.findById(id);
        if(optionalWallet.isPresent()) {
            Wallet wallet = optionalWallet.get();
            wallet.setCryptoGuthaben(cryptoGuthaben.getCryptoGuthaben());
            this.walletJPARepo.
            return wallet;
        }else{
            throw new WalletNichtGefunden("Das Wallet mit der id" + id + " wurde nicth gefunden");
        }
    }*/


    /*public Wallet cryptoGuthabenEntfernenMitId(Long id, CryptoGuthaben cryptoGuthaben) {
        return null;
    }*/
}
