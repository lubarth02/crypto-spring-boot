package at.itkolleg.cryptospringboot.exceptions;

public class BenutzerValidierungFehlgeschlagen extends Exception {

    private FormValidierungExceptionDTO errors;

    public BenutzerValidierungFehlgeschlagen(String message)
    {
        super(message);
    }

    public BenutzerValidierungFehlgeschlagen(FormValidierungExceptionDTO errors)
    {
        this.errors = errors;
    }

    public FormValidierungExceptionDTO getErrorMap()
    {
        return errors;
    }
}


