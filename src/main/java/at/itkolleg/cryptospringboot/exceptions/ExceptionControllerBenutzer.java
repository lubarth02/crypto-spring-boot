package at.itkolleg.cryptospringboot.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionControllerBenutzer {

    @ExceptionHandler(BenutzerNichtGefunden.class)
    public ResponseEntity<ExceptionDTO> benutzerNichtGefunden(BenutzerNichtGefunden benutzerNichtGefunden) {
        return new ResponseEntity<>(new ExceptionDTO("1000", benutzerNichtGefunden.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BenutzerValidierungFehlgeschlagen.class)
    public ResponseEntity<FormValidierungExceptionDTO> benutzerValidierungFehlgeschlagen(BenutzerValidierungFehlgeschlagen benutzerValidierungFehlgeschlagen) {
        return new ResponseEntity<>(benutzerValidierungFehlgeschlagen.getErrorMap(), HttpStatus.BAD_REQUEST);
    }
}

