package at.itkolleg.cryptospringboot.exceptions;

public class InvalidBenutzerGuthaben extends Exception{
    public InvalidBenutzerGuthaben(String message){
        super(message);
    }
}
