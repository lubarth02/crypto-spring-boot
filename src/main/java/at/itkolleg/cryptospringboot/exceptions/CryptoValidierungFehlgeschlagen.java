package at.itkolleg.cryptospringboot.exceptions;

public class CryptoValidierungFehlgeschlagen extends Exception {

    private FormValidierungExceptionDTO errors;

    public CryptoValidierungFehlgeschlagen(String message)
    {
        super(message);
    }

    public CryptoValidierungFehlgeschlagen(FormValidierungExceptionDTO errors)
    {
        this.errors = errors;
    }

    public FormValidierungExceptionDTO getErrorMap()
    {
        return errors;
    }
}

