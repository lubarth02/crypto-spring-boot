package at.itkolleg.cryptospringboot.exceptions;

public class WalletNichtGefunden extends Exception{
    public WalletNichtGefunden(String message){
        super(message);
    }
}
