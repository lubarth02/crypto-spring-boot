package at.itkolleg.cryptospringboot.exceptions;

public class BenutzerNichtGefunden extends Exception{
    public BenutzerNichtGefunden(String message) {
        super(message);
    }
}


