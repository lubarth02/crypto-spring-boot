package at.itkolleg.cryptospringboot.exceptions;

public class CryptoNichtGefunden extends Exception{

    public CryptoNichtGefunden(String message) {
        super(message);
    }
}

