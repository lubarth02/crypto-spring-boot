
package at.itkolleg.cryptospringboot.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionControllerCrypto {

    @ExceptionHandler(CryptoNichtGefunden.class)
    public ResponseEntity<ExceptionDTO> cryptoNichtGefunden(CryptoNichtGefunden cryptoNichtGefunden) {
        return new ResponseEntity<>(new ExceptionDTO("1000", cryptoNichtGefunden.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CryptoValidierungFehlgeschlagen.class)
    public ResponseEntity<FormValidierungExceptionDTO> cryptoValidierungFehlgeschlagen(CryptoValidierungFehlgeschlagen cryptoValidierungFehlgeschlagen) {
        return new ResponseEntity<>(cryptoValidierungFehlgeschlagen.getErrorMap(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CryptoDeletionNotPossibleException.class)
    public ResponseEntity<ExceptionDTO> cryptoDeletionNotPossible(CryptoDeletionNotPossibleException studentDeletionNotPossibleException) {
        return new ResponseEntity<>(new ExceptionDTO("5000", studentDeletionNotPossibleException.getMessage()), HttpStatus.CONFLICT);
    }
}
