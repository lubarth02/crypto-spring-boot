package at.itkolleg.cryptospringboot.exceptions;

public class InvalidCryptoGuthabenException extends Exception {

    public InvalidCryptoGuthabenException(String message){
        super(message);
    }
}
