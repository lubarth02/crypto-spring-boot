package at.itkolleg.cryptospringboot.exceptions;

public class InvalidPreisException extends Exception {
    public InvalidPreisException(String message) {
        super(message);
    }
}


