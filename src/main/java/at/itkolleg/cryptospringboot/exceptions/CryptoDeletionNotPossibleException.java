package at.itkolleg.cryptospringboot.exceptions;

public class CryptoDeletionNotPossibleException extends Exception{

    public CryptoDeletionNotPossibleException(String message) {
        super(message);
    }
}
