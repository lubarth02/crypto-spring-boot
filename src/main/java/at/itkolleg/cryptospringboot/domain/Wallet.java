package at.itkolleg.cryptospringboot.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    private Crypto crypto;

    @ManyToOne(fetch = FetchType.EAGER)
    private Benutzer benutzer;

    private String name;

    //private double mengeAnCrypto;
    private double cryptoGuthaben;

    public Wallet(Crypto crypto, Benutzer benutzer, String name, double cryptoGuthaben) {
        setCrypto(crypto);
        setBenutzer(benutzer);
        setName(name);
        setCryptoGuthaben(cryptoGuthaben);
    }

    public Wallet(Crypto crypto, Benutzer benutzer) {
        setCrypto(crypto);
        setBenutzer(benutzer);
        setName("");
        setCryptoGuthaben(0);
    }

    public void setBenutzer(Benutzer benutzer) {
        this.benutzer = benutzer;
    }

    public Benutzer getBenutzer() {
        return this.benutzer;
    }

    public void setCryptoGuthaben(double cryptoGuthaben) {
        this.cryptoGuthaben = cryptoGuthaben;
    }

    public double getCryptoGuthaben() {
        return this.cryptoGuthaben;
    }

    public Crypto getCrypto() {
        return crypto;
    }

    //Vielleicht kein setter für crypto, weil crypto nur eine Wallet hat
    public void setCrypto(Crypto crypto) {
        this.crypto = crypto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() < 1 || name.length() > 50) {
            this.name = crypto.getName() + " Wallet";
        } else {
            this.name = name;
        }
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Double calculateEuroPreis(double cryptoPreis) {
        Double euroPreis = cryptoPreis * this.cryptoGuthaben;
        Double gerundet = Math.round(euroPreis * 100.0) / 100.0;
        return gerundet;
    }

    public Boolean walletActive() {
        if (this.cryptoGuthaben > 0) {
            return true;
        }
        return false;
    }

    public void addCryptoGuthaben(Double cryptoGuthaben) {
        Double derzeitigesGuthaben = this.cryptoGuthaben;
        this.setCryptoGuthaben(derzeitigesGuthaben + cryptoGuthaben);
    }

}
