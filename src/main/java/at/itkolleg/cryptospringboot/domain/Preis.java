package at.itkolleg.cryptospringboot.domain;

import at.itkolleg.cryptospringboot.exceptions.InvalidPreisException;
import jakarta.persistence.Embeddable;

@Embeddable
public class Preis {

    private double preis;

    public Preis(double preis) throws InvalidPreisException {
        if (preis < 0) {
            throw new InvalidPreisException("Der Preis darf nicht negativ sein");
        } else {
            this.preis = preis;
        }
    }

    public Preis() {

    }

    public double getPreis() {
        return this.preis;
    }

    // Wieso setter mit new Aufruf?
    public void setPreis(double value) throws InvalidPreisException {
        new Preis(value);
    }

    public boolean equals(Preis other) {
        return this.preis == other.preis;
    }

}

