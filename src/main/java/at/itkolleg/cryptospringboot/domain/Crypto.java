package at.itkolleg.cryptospringboot.domain;

import at.itkolleg.cryptospringboot.exceptions.InvalidPreisException;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Crypto {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Size(min = 2, max = 255, message = "Der Name muss zwischen 2 und 255 Zeichen lang sein")
    private String name;

    //@Size(min = 0, message = "Die Kryptowährung muss einen Preis haben")
    //private Preis preis; durch double ersetzt um Fehler eim einfügen udn upadten zu umgehen
    private double preis;

    public Crypto(String name, double preis){
        setName(name);
        setPreis(preis);
        //Preis preisObj = new Preis(preis);
        //setPreis(preisObj);
    }

    public Long getId() {
        return id;
    }
}



