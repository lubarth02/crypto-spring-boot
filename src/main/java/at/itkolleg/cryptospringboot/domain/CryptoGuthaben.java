package at.itkolleg.cryptospringboot.domain;

import at.itkolleg.cryptospringboot.exceptions.InvalidCryptoGuthabenException;
import jakarta.persistence.Embeddable;

@Embeddable
public class CryptoGuthaben {

    private double cryptoGuthaben;

    public CryptoGuthaben(double cryptoGuthaben) throws InvalidCryptoGuthabenException{
        setCryptoGuthaben(cryptoGuthaben);
    }

    public CryptoGuthaben(){

    }

    private void setCryptoGuthaben(double cryptoGuthaben) throws InvalidCryptoGuthabenException {
        if(cryptoGuthaben < 0){
            throw new InvalidCryptoGuthabenException("Das Crypto Guthaben darf nicht negativ sein!");
        } else {
            //Double gerundet = Math.round(cryptoGuthaben * 100000.0) / 100000.0;
            this.cryptoGuthaben = cryptoGuthaben;
        }
    }

    public double getCryptoGuthaben(){
        return this.cryptoGuthaben;
    }
}
