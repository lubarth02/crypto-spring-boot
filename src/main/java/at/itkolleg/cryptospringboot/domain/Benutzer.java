package at.itkolleg.cryptospringboot.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Benutzer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Size(min = 2, max = 255, message = "Der Username '${validatedValue}' ist ungültig. Usernamen müssen zwischen {min} und {max} Zeichen lang sein.")
    private String name;

    @Size(min = 4, max = 20, message = "Die Email-Adresse '${validatedValue}' ist ungültig. Email-Adressen müssen zwischen {min} und {max} Zeichen lang sein.")
    private String email;

    private Double guthaben;

    public Benutzer(String name, String email, Double guthaben) {
        setName(name);
        setEmail(email);
        setGuthaben(guthaben);
    }

    public Double getGuthaben() {
        return guthaben;
    }

    public void setGuthaben(Double guthaben) {
        Double gerundet = Math.round(guthaben * 100.0) / 100.0;
        this.guthaben = gerundet;
    }


}

