# Crypto-Spring-Boot

Projekt für FSE von Armin Hamzic und Lukas Barth. 

[TOC]

## 01_Einleitung

Es wird ein Fullstack-Projekt über Kryptowährung-Wallets umgesetzt. Dabei wird Spring-Boot verwendet mit dem Framework Thymeleaf.  Es ist möglich alle Kryptowährungen anzeigen zu lassen und ausgewählte Kryptowährungen in bestimmte Wallets einzufügen. Weiters wird es möglich sein, neue Wallets hinzuzufügen und in bestehende Wallets einzusehen. 

## 02_Umsetzung



